package com.foodsearch.utils

enum class Status {
    LOADING,
    SUCCESS,
    ERROR;
}