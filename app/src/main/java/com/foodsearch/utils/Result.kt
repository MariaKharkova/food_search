package com.foodsearch.utils

/**
 * Class for handling fetching data
 * @param status status of the response
 * @param response response value
 * @param error if response failed contains information about the error, otherwise null
 */
data class Result<T>(var status: Status, var response: T? = null, var error: Error? = null) {

    companion object {

        fun <T> success(response: T?): Result<T> =
            Result(Status.SUCCESS, response)

        fun <T> loading(): Result<T> = Result(Status.LOADING)

        fun <T> error(error: Error?): Result<T> =
            Result(Status.ERROR, error = error)
    }
}