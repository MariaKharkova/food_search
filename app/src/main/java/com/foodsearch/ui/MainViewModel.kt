package com.foodsearch.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.foodsearch.utils.Result
import com.foodsearch.domain.GetPlacesUseCase
import com.foodsearch.domain.Place
import com.google.android.gms.maps.model.LatLng

class MainViewModel(private val getPlacesUseCase: GetPlacesUseCase) : ViewModel() {

    private val currentLocation: MutableLiveData<LatLng> = MutableLiveData()

    val foodPlaces: LiveData<Result<List<Place>>> = currentLocation.switchMap {
        getPlacesUseCase.getPlaces(it.longitude, it.latitude)
    }

    fun updateCurrentLocation(location: LatLng) {
        currentLocation.postValue(location)
    }
}