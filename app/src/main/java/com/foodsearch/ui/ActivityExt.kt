package com.foodsearch.ui

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager

const val REQUEST_LOCATION_LOCATION = 100

fun Activity.requestCheckPermissions(requestCodes: Int): Boolean {

    val missingPermissions = ArrayList<String>()
    val requiredPermissions = getPermissionsNeeded(requestCodes)

    for (permission: String in requiredPermissions) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            missingPermissions.add(permission)
        }
    }

    if (missingPermissions.size > 0) {
        val permissions = arrayOfNulls<String>(missingPermissions.size)
        requestPermissions(missingPermissions.toArray(permissions), requestCodes)
        return false
    }

    return true
}


private fun getPermissionsNeeded(requestCode: Int): ArrayList<String> {

    val permissions: ArrayList<String> = ArrayList()

    when (requestCode) {

        REQUEST_LOCATION_LOCATION -> {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }

    return permissions

}