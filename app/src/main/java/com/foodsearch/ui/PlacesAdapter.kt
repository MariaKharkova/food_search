package com.foodsearch.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.foodsearch.databinding.ItemListPlaceBinding
import com.foodsearch.domain.Place

class PlacesAdapter : ListAdapter<Place, PlacesAdapter.PlaceViewHolder>(DiffCallback()) {

    private var places: List<Place> = emptyList()

    override fun submitList(list: List<Place>?) {
        places = list ?: emptyList()
        super.submitList(list)
    }

    override fun getItemCount() = places.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PlaceViewHolder.from(parent)

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.bind(places[position])
    }

    class PlaceViewHolder(private val binding: ItemListPlaceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): PlaceViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemListPlaceBinding.inflate(layoutInflater, parent, false)
                return PlaceViewHolder(binding)
            }
        }

        fun bind(place: Place) {
            binding.apply {
                placeName.text = place.placeName
                placeAddress.text = place.placeAddress
            }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Place>() {
        override fun areItemsTheSame(oldItem: Place, newItem: Place) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Place, newItem: Place) = oldItem == newItem
    }
}