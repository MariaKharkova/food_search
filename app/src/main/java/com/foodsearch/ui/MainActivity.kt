package com.foodsearch.ui

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.foodsearch.R
import com.foodsearch.utils.Result
import com.foodsearch.utils.Status
import com.foodsearch.domain.Place
import com.foodsearch.databinding.ActivityMainBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private val mainViewModel: MainViewModel by viewModel()

    private lateinit var binding: ActivityMainBinding

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var map: GoogleMap? = null

    private lateinit var placesAdapter: PlacesAdapter

    private val placeObserver: Observer<Result<List<Place>>> = Observer {
        binding.progress.visibility = if (it.status == Status.LOADING) View.VISIBLE else View.GONE
        when (it.status) {
            Status.SUCCESS -> {
                it.response?.let { data -> updateUIWithData(data) }
            }
            Status.ERROR -> Toast.makeText(this, it.error?.message, Toast.LENGTH_SHORT).show()
            Status.LOADING -> {
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        setContentView(binding.root)
        initViews()

        mainViewModel.foodPlaces.observe(this, placeObserver)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return
            }
        }
        when (requestCode) {
            //permissions were granted , can init map
            REQUEST_LOCATION_LOCATION -> setupGoogleMap()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        if (requestCheckPermissions(REQUEST_LOCATION_LOCATION)) {
            setupGoogleMap()
        }
    }

    private fun initViews(){
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        placesAdapter = PlacesAdapter()
        binding.placesList.adapter = placesAdapter
    }

    @SuppressLint("MissingPermission")
    private fun setupGoogleMap() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { lastLocation: Location? ->
                lastLocation?.let { location ->
                    val currentLocation = LatLng(location.latitude, location.longitude)
                    map?.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 14f))
                    mainViewModel.updateCurrentLocation(currentLocation)
                }
            }

    }

    private fun updateUIWithData(places: List<Place>) {
        places.forEach { place ->
            map?.addMarker(
                MarkerOptions().position(LatLng(place.latitude, place.longitude))
                    .title(place.placeName)
            )
            placesAdapter.submitList(places)
        }
    }
}