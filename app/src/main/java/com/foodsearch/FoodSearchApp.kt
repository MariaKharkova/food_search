package com.foodsearch

import android.app.Application
import com.foodsearch.di.Modules
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class FoodSearchApp : Application() {
    override fun onCreate() {
        super.onCreate()
        initRealm()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@FoodSearchApp)
            modules(Modules.modulesList)
        }
    }

    private fun initRealm() {
        Realm.init(this@FoodSearchApp)
        val config = RealmConfiguration.Builder()
            .name("places")
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config);
    }
}