package com.foodsearch.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.foodsearch.data.local.PlacesDao
import com.foodsearch.data.network.ApiInterface
import com.foodsearch.data.network.responses.PlacesResponse
import com.foodsearch.utils.Result
import kotlinx.coroutines.Dispatchers

class PlacesRepository(private val apiInterface: ApiInterface, private val placesDao: PlacesDao) :
    GetPlacesUseCase {

    override fun getPlaces(longitude: Double, latitude: Double): LiveData<Result<List<Place>>> =
        liveData(Dispatchers.IO) {
            emit(Result.loading())
            val daoResults = placesDao.fetchPlaces()

            // Try to get data from db
            if (daoResults.isNotEmpty()) {
                emit(Result.success(daoResults))
            } else {
                // Fetch from network
                val response = fetchFromNetwork(longitude, latitude)
                response.response?.let { placesDao.addPlaces(it) }
                emit(response)
            }
        }

    private suspend fun fetchFromNetwork(longitude: Double, latitude: Double): Result<List<Place>> {
        val response = try {
            apiInterface.findPlaces(
                category = "Food",
                location = "$longitude, $latitude",
                maxLocations = 20,
                outFields = "Place_addr, PlaceName"
            )
        } catch (e: Exception) {
            return Result.error(java.lang.Error(e.message))
        }

        if (response.errorBody() != null) {
            return Result.error(java.lang.Error(response.message()))
        }

        return Result.success(buildPlacesList(response.body()))
    }

    private fun buildPlacesList(response: PlacesResponse?): List<Place> {
        val places = arrayListOf<Place>()
        response?.candidates?.forEach { place ->
            places.add(
                Place(
                    latitude = place.location.y,
                    longitude = place.location.x,
                    placeName = place.attributes.placeName,
                    placeAddress = place.attributes.address
                )
            )
        }
        return places
    }
}