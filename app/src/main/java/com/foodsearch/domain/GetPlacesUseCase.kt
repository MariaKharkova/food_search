package com.foodsearch.domain

import androidx.lifecycle.LiveData
import com.foodsearch.utils.Result

interface GetPlacesUseCase {

    fun getPlaces(longitude: Double, latitude: Double): LiveData<Result<List<Place>>>
}