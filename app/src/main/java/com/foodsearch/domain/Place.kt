package com.foodsearch.domain

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Place constructor(
    @PrimaryKey
    var id: Int? = 0,
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var placeName: String? = null,
    var placeAddress: String? = null
) : RealmObject()