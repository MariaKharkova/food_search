package com.foodsearch.di

import com.foodsearch.BuildConfig
import com.foodsearch.data.local.PlacesDao
import com.foodsearch.data.local.PlacesDaoImpl
import com.foodsearch.data.network.ApiInterface
import com.foodsearch.domain.GetPlacesUseCase
import com.foodsearch.domain.PlacesRepository
import com.foodsearch.ui.MainViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Modules {

    private const val DEFAULT_TIMEOUT: Long = 30

    private val viewModelsModule = module {
        viewModel { MainViewModel(get()) }
    }

    private val repositoriesModule = module {

        fun provideGetPlacesUseCase(
            apiInterface: ApiInterface,
            placesDao: PlacesDao
        ): GetPlacesUseCase {
            return PlacesRepository(apiInterface, placesDao)
        }

        single { provideGetPlacesUseCase(get(), get()) }
    }

    private val databaseModule = module {

        fun providePlacesDao(): PlacesDao {
            return PlacesDaoImpl()
        }

        factory { providePlacesDao() }
    }

    private val networkModule = module {
        fun provideRetrofit(okHttpClient: OkHttpClient) =
            Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        fun provideOkHttpClient(loggerInterceptor: HttpLoggingInterceptor): OkHttpClient {
            val httpClient = OkHttpClient.Builder()

            httpClient.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            httpClient.readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            httpClient.writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG)
                httpClient.addInterceptor(loggerInterceptor)

            return httpClient.build()
        }

        single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }
        single { provideOkHttpClient(get()) }
        single { provideRetrofit(get()) }
        single { get<Retrofit>().create(ApiInterface::class.java) }
    }

    val modulesList = listOf(
        databaseModule,
        repositoriesModule,
        networkModule,
        viewModelsModule
    )
}