package com.foodsearch.data.network

import com.foodsearch.data.network.responses.PlacesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("findAddressCandidates?f=json")
    suspend fun findPlaces(
        @Query("category") category: String,
        @Query("location") location: String,
        @Query("outFields") outFields: String,
        @Query("maxLocations") maxLocations: Int
    ): retrofit2.Response<PlacesResponse>

}