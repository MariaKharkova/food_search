package com.foodsearch.data.network.responses

import com.google.gson.annotations.SerializedName

data class PlacesResponse(val spatialReference: SpatialReference, val candidates: List<Candidate>)

data class SpatialReference(
    @field:SerializedName("wkid") val wkId: Int,
    @field:SerializedName("latestWkid") val latestWkId: Int
)

data class Candidate(
    val address: String,
    val location: Location,
    val score: Int,
    val attributes: Attributes,
    val extent: Extent
)

data class Location(val x: Double, val y: Double)

data class Attributes(
    @field:SerializedName("PlaceName") val placeName: String,
    @field:SerializedName("Place_addr") val address: String
)

data class Extent(
    @field:SerializedName("xmin") val xMin: Double,
    @field:SerializedName("ymin") val yMin: Double,
    @field:SerializedName("xmax") val xMax: Double,
    @field:SerializedName("ymax") val yMax: Double
)

