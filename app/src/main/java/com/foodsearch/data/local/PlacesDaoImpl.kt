package com.foodsearch.data.local

import android.util.Log
import com.foodsearch.domain.Place
import io.realm.Realm

class PlacesDaoImpl : PlacesDao{

    private val TAG = "PlacesDao"

    private var uniqueId = 0

    override fun addPlaces(places: List<Place>): Boolean {
        return try {
            val realm = Realm.getDefaultInstance()
            places.forEach {
                it.id = (setUniqueId())
            }
            realm.executeTransaction{realm1 ->
                realm1.insertOrUpdate(places)
            }
            realm.close()
            true
        } catch (e: Exception) {
            Log.d(TAG, e.message?:"")
            false
        }
    }

    override fun fetchPlaces(): List<Place> {
        val realm = Realm.getDefaultInstance()
        val list =  realm.copyFromRealm(realm.where(Place::class.java).findAll())
        realm.close()
        return list
    }


    private fun setUniqueId(): Int {
        uniqueId = uniqueId.inc()
        return uniqueId
    }

}