package com.foodsearch.data.local

import com.foodsearch.domain.Place

interface PlacesDao {

    fun addPlaces(places: List<Place>): Boolean

    fun fetchPlaces(): List<Place>
}